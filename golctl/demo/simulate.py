from time import sleep
import random

def randsleep(tmax=10, tmin=1):
    sleep(random.uniform(tmin, tmax))

def randfail(p=0.05):
    if random.random() < p:
        raise RuntimeError('Failure!!!')

