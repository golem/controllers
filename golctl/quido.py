"""Simple Quido relay control using the Spinel 66 protocol"""

from time import sleep
from socket import create_connection


def relay(address, pin, state, wait=0.1):
    state_str = 'H' if state else 'L'
    with create_connection((address, 10001)) as c:
        c.send('*B1OS{}{}'.format(pin, state_str))
    if wait:
        sleep(wait)

