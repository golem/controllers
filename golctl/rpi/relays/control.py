# from Seeed Studio Wiki
# http://wiki.seeed.cc/Raspberry_Pi_Relay_Board_v1.0/


class RelayBoards():

    def __init__(self, bus_address=1, *board_addresses):
        import smbus
        # 0 = /dev/i2c-0 (port I2C0), 1 = /dev/i2c-1 (port I2C1)
        self.bus = smbus.SMBus(bus_address)
        self.boards = [RelayBoard(self.bus, addr) for addr in board_addresses]

    def __getitem__(self, i):
        return self.boards[i]

    def __len__(self):
        return len(self.boards)


class RelayBoard():

    n_relays = 4

    def __init__(self, bus, board_address=0x20):
        import smbus
        if not isinstance(bus, smbus.SMBus):
            bus = smbus.SMBus(bus)
        self.bus = bus
        self.DEVICE_ADDRESS = board_address  # 7 bit address (will be left shifted to add the read write bit)
        self.DEVICE_REG_MODE1 = 0x06
        self.DEVICE_REG_DATA = 0xff
        self.get_state()

    def get_state(self):
        self.DEVICE_REG_DATA = self.bus.read_byte_data(self.DEVICE_ADDRESS, self.DEVICE_REG_MODE1)

    def set_state(self):
        self.bus.write_byte_data(self.DEVICE_ADDRESS, self.DEVICE_REG_MODE1, self.DEVICE_REG_DATA)

    def is_on(self, i):
        self.get_state()
        return (self.DEVICE_REG_DATA & (0x1 << i)) == 0

    def on(self, i, sync=True):
        if sync:
            self.get_state()
        self.DEVICE_REG_DATA &= ~(0x1 << i)
        self.set_state()

    def off(self, i, sync=True):
        if sync:
            self.get_state()
        self.DEVICE_REG_DATA |= (0x1 << i)
        self.set_state()

    def flip(self, i):
        if self.is_on(i):
            self.off(i, sync=False) # already know state from above
        else:
            self.on(i, sync=False)

    def all_on(self):
        self.DEVICE_REG_DATA &= ~(0xf << 0)
        self.set_state()

    def all_off(self):
        self.DEVICE_REG_DATA |= (0xf << 0)
        self.set_state()
