from time import sleep

from .control import RelayBoard


def blink(relay_i, i2c_bus=1, board_address=0x20, sleep_time=1):
        relay = RelayBoard(i2c_bus, board_address)
        relay.on(relay_i)
        sleep(sleep_time)
        relay.off(relay_i)
        sleep(sleep_time)
        return relay_i


def next_relay(i):
    return (i+1) % RelayBoard.n_relays
