#!/usr/bin/env python3

# written for Python 3

import tkinter as tk
from tkinter.font import Font


from .control import RelayBoards, RelayBoard

relay_boards = RelayBoards(1, 0x20, 0x23)




class Application(tk.Frame):

    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.pack()
        self.font = Font(size=30)
        self.buttons = {}

        for row in range(len(relay_boards)):
            for col in range(RelayBoard.n_relays):
                button = tk.Button(self, text='Rele {}-{}'.format(row+1, col+1),
                        command=lambda row=row, col=col: self.click_relay_button(row, col),
                        font=self.font,
                        )
                button.grid(row=col, column=row)
                self.buttons[(row, col)] = button
                self.colorize_relay_button(row, col)

    def click_relay_button(self, row, col):
        r = relay_boards[row]
        r.flip(col)
        self.colorize_relay_button(row, col)

    def colorize_relay_button(self, row, col):
        r = relay_boards[row]
        button = self.buttons[(row, col)]
        color = 'green' if r.is_on(col) else 'red'
        button.configure(bg=color, activebackground=color)


if __name__ == '__main__':
    root = tk.Tk()
    app = Application(master=root)
    app.mainloop()
