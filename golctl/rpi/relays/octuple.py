"""8 Relays Module"""

from time import sleep
import RPi.GPIO as GPIO



# TODO may be set too often
GPIO.setmode(GPIO.BCM)


def relay_state(pin, state, wait=0.1):
    """Set given relay pin to state (True - ON, False - OFF)

    Optionally wait for *wait* seconds for hardware to operate
    """
    GPIO.setup(pin, GPIO.OUT)  # TODO necessary each time?
    # Note: inverse relation, LOW is ON
    state = GPIO.LOW if state else GPIO.HIGH
    GPIO.output(pin, state)
    if wait:
        sleep(wait)



def relays(*pin_states, **kwargs):
    """Set each pin (int) to specified state (+ ON, - OFF)


    Example:
    --------
    relays(+1, -3)   # set relay 1 ON, relay 3 OFF
    """
    for p in pin_states:
        pin = abs(p)
        relay_state(abs(p), p > 0, **kwargs)
