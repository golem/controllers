"""Requires installation of ADC-HAT from Seeed/pi-hats"""


# TODO detect correct hex in path
def read_raw(input_n):
    with open('/sys/devices/platform/soc/3f804000.i2c/i2c-1/1-0048/in{}_input'.format(input_n), 'r') as reg:
        value = reg.read()
    return int(value)
