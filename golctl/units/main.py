from . import el_field, chamber
from ..demo.simulate import randsleep

def arm_das(preion_on):
    randsleep(10, 11)
    return True

def secure_tokamak(Et_secure, chamber_evacuated):
    return True

def process_data(tokamak_secure):
    randsleep(5, 5)
    return 'finito'

def discharge_ready(charged_Et, chamber_filled):
    return True


def create_discharge_dsk(U_Et, wg_pressure, gas_type,
                         Et_controller_ip, chamber_controller_ip, other_ips):
    dsk = {}
    dsk['fill_chamber'] = (chamber.fill_working_gas, wg_pressure, gas_type)
    dsk['charge_Et'] = (el_field.charge_Et_capacitors, U_Et)
    dsk['discharge_ready'] = (discharge_ready, 'charge_Et', 'fill_chamber')
    dsk['preion'] = (chamber.activate_prionization, 'discharge_ready')
    dsk['arm_das'] = (arm_das, 'preion')
    dsk['trigger'] = (chamber.trigger, 'arm_das')
    dsk['secure_Et'] = (el_field.secure_Et_capacitors, 'trigger')
    dsk['preion_off'] = (chamber.deactivate_prionization, 'trigger')
    dsk['evacuate_chamber'] = (chamber.evacuate, 'preion_off')
    dsk['secure_tokamak'] = (secure_tokamak, 'secure_Et', 'evacuate_chamber')
    dsk['process_data'] = (process_data, 'secure_tokamak')

    restrictions = {}
    for i in ('charge_Et', 'secure_Et'):
        restrictions[i] = {Et_controller_ip}
    for i in ('fill_chamber', 'preion', 'trigger', 'preion_off', 'evacuate_chamber'):
        restrictions[i] = {chamber_controller_ip}
    for i in dsk.keys():
        if i not in restrictions:
            restrictions[i] = other_ips

    return dsk, 'process_data', restrictions
