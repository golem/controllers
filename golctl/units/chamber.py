from ..demo.simulate import randsleep, randfail

def fill_working_gas(pressure, gas_type):
    randsleep(4, 6)
    return pressure

def activate_prionization(discharge_ready):
    randsleep(1, 1.5)
    return 1


def deactivate_prionization(trigger):
    return 0

def evacuate(preionization_off):
    randsleep(1, 2)
    return 0

def trigger(das_armed):
    return True # TODO move to proper unit
