from ..rpi.relays.octuple import relay_state
from ..rpi.adc.hat import read_raw
from ..quido import relay as quido_relay

from time import sleep


def high_voltage_light(active):
    relay_state(12, active)


def high_voltage(active):
    quido_relay('192.168.2.250', 3, active)
    high_voltage_light(active)
    sleep(1)


def short_circuit(active=True):
    if not active:
        high_voltage(False)     # sleeps 1 inside
    relay_state(18, not active)  # default LOW state == short circuit active


def trigger():
    relay_state(6, True)
    sleep(0.5)
    relay_state(6, False)


def charging(active):
    relay_state(17, active)


def charge_for(seconds):
    short_circuit(False)
    sleep(1)
    charging(True)
    sleep(seconds)
    charging(False)
    sleep(0.5)
